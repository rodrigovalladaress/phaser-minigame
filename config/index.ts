import { Size } from "../src/interface/size";

const BASE_SIZE = <Size>{
  width: 384,
  height: 512
};

/**
  Configuration values of the game.
 */
export class Config {
  public static readonly SCREEN = Object.freeze({
    BASE_SIZE: <Size>Object.freeze(BASE_SIZE),
    RATIO: <number>BASE_SIZE.width / BASE_SIZE.height
  });

  public static readonly MISSILE = Object.freeze({
    COUNT: <number>3,
    OFFSET: <number>30,
    MAX_DRAG_DISTANCE: <number>50,
    SPEED_MULTIPLIER: <number>1000,
    MIN_SPEED: <number>400,
    MAX_SPEED: <number>800,
    BOUNCE: Object.freeze({
      X: <number>0.95,
      Y: <number>0.95
    })
  });

  public static readonly ENEMY = Object.freeze({
    INITIAL_MAX_NUM: <number>5,
    SPEED_STEP: <number>0.15,
    TICK_STEP: <number>10,
    UFO: Object.freeze({
      speed: Object.freeze({ x: 0, y: 7 }),
      score: 1,
      harm: 1
    }),
    ALIEN: Object.freeze({
      speed: Object.freeze({ x: 5, y: 7 }),
      score: 5,
      harm: 5,
      secondsToChangeDirection: 3
    })
  });

  public static readonly SCORE = Object.freeze({
    COLOR: <string>"white"
  });

  public static readonly SHIELD = Object.freeze({
    LIFE: <number>100,
    PORCENTUAL_DISTANCE: <number>0.75
  });

  public static readonly MENU = Object.freeze({
    WINDOW: Object.freeze({
      COLOR: <number>0xffffff,
      ALPHA: <number>0.7,
      LINE_COLOR: <number>0xff0000,
      LINE_ALPHA: <number>1
    }),
    RETRY_BUTTON: Object.freeze({
      SIZE: <Size>Object.freeze({ width: 100, height: 50 }),
      COLOR: <number>0xfafa12,
      ALPHA: <number>1,
      LINE_COLOR: <number>0x000000,
      LINE_ALPHA: <number>1,
      TEXT: Object.freeze({
        CONTENT: <string>"Retry",
        COLOR: <string>"black"
      })
    }),
    GAME_OVER_TEXT: Object.freeze({
      CONTENT: <string>"Game over",
      COLOR: <string>"red"
    })
  });
}
