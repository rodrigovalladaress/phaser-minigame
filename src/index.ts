import "p2";
import "pixi";
import "phaser";

import { GameManager } from "./gameManager";

const gameConfig: Phaser.IGameConfig = {
  renderer: Phaser.AUTO
};

new GameManager(gameConfig);
