import { Keys } from "../assets";
import { Size } from "../interface/size";
import { Enemy } from "./enemy";
import { Config } from "../../config";

/**
  Enemy with an UFO sprite that moves down.
 */
export class Ufo extends Enemy {
  constructor(game: Phaser.Game, physicsSystem: number) {
    super(game, physicsSystem, Keys.ufo, {
      width: 2.5,
      height: 2.5
    });
  }

  protected init(scale: Size) {
    this.ySpeed = Config.ENEMY.UFO.speed.y;
    this.score = Config.ENEMY.UFO.score;
    this.harm = Config.ENEMY.UFO.harm;

    super.init(scale);
  }
}
