// https://medium.com/@buddhi.amigo/how-to-create-typescript-classes-dynamically-b29ca7767ee5
import { Ufo } from "./ufo";
import { Alien } from "./alien";

/**
  Types of enemies.
 */
export const Types: { [id: string]: any } = {
  Ufo,
  Alien
};
