import { Keys } from "../assets";
import { Size } from "../interface/size";
import { Enemy } from "./enemy";
import { Config } from "../../config";

/**
  Enemy with an alien sprite that moves down and changes the x direction from
  time to time.
 */
export class Alien extends Enemy {
  private lastChangeDate: number = Date.now();
  private timeSinceLastChange: number = 0;
  protected timeToChange: number =
    Config.ENEMY.ALIEN.secondsToChangeDirection * 1000;

  constructor(game: Phaser.Game, physicsSystem: number) {
    super(game, physicsSystem, Keys.alien, {
      width: 2.5,
      height: 2.5
    });

    this.timeToChange += Phaser.Math.random(0, 500);
  }

  protected init(scale: Size) {
    const xDirection = Math.sign(Phaser.Math.random(-1, 1));
    this.ySpeed = Config.ENEMY.ALIEN.speed.y;
    this.xSpeed = Config.ENEMY.ALIEN.speed.x * xDirection;
    this.score = Config.ENEMY.ALIEN.score;
    this.harm = Config.ENEMY.ALIEN.harm;

    super.init(scale);
  }

  protected changeXDirection() {
    this.xSpeed *= -1;
    this.updateSpeed();

    this.timeSinceLastChange = 0;
    this.lastChangeDate = Date.now();
  }

  public update() {
    this.timeSinceLastChange = Date.now() - this.lastChangeDate;

    if (this.timeSinceLastChange >= this.timeToChange) {
      this.changeXDirection();
    }

    super.update();
  }
}
