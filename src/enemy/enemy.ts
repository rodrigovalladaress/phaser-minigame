import { Size } from "../interface/size";

/**
  The reason the enemy has been destroyed (by a missile or by attacking the
  shield?).
*/
export enum DestroyedReason {
  missile,
  shield
}

/**
  Enemy base class.
 */
export abstract class Enemy extends Phaser.Sprite {
  public readonly halfWidth: number;
  public readonly halfHeight: number;

  protected speedMultiplier: number = 1;
  protected xSpeed: number = 0;
  protected ySpeed: number = 0;
  protected readonly physicsSystem: number;
  public onDestroyed: (enemy: Enemy, reason: DestroyedReason) => any;
  /**
    Score given to the player when the enemy is defeated.
   */
  protected score: number;
  /**
    Damage that causes the enemy to the shield.
   */
  protected harm: number;

  constructor(
    game: Phaser.Game,
    physicsSystem: number,
    sprite: string,
    scale: Size = { width: 1, height: 1 },
    x: number = 0,
    y: number = 0
  ) {
    super(game, x, y, sprite);

    this.halfWidth = this.width / 2;
    this.halfHeight = this.width / 2;

    this.physicsSystem = physicsSystem;
    this.init(scale);
  }

  protected init(scale: Size) {
    this.scale.setTo(scale.width, scale.height);

    this.game.physics.enable(this, this.physicsSystem);
    this.game.add.existing(this);
    this.game.physics.enable(this, this.physicsSystem);
    this.body.collideWorldBounds = true;
    this.updateSpeed();
  }

  public getScore(): number {
    return this.score;
  }

  public getHarm(): number {
    return this.harm;
  }

  public onMissileCollision() {
    this.destroyEnemy(DestroyedReason.missile);
  }

  public onShieldCollision() {
    this.destroyEnemy(DestroyedReason.shield);
  }

  /**
    Updates the speed according to the speedMultiplier.
   */
  protected updateSpeed() {
    this.body.velocity.y = this.ySpeed * this.speedMultiplier;
    this.body.velocity.x = this.xSpeed * this.speedMultiplier;
  }

  public setSpeedMultiplier(speedMultiplier: number) {
    this.speedMultiplier = speedMultiplier;
    this.updateSpeed();
  }

  public destroyEnemy(reason: DestroyedReason) {
    if (this.onDestroyed) {
      this.onDestroyed(this, reason);
    }
    this.destroy();
  }
}
