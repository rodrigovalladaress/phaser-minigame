import { Size } from "../interface/size";

/**
  Calculates the size of the game in order to maintain the ratio of the game
  size.
 */
export class ScreenSizeHelper {
  public readonly gameRealSize: Size;

  constructor(ratio: number) {
    const width = Math.floor(window.innerWidth);
    const height = Math.floor(window.innerHeight);
    const windowSize = {
      width,
      height
    };
    const realSize = { width: 0, height: 0 };

    const windowRatio = windowSize.width / windowSize.height;

    if (windowRatio >= ratio) {
      realSize.width = windowSize.height * ratio;
      realSize.height = windowSize.height;
    } else {
      realSize.width = windowSize.width;
      realSize.height = windowSize.width / ratio;
    }

    this.gameRealSize = realSize;
  }
}
