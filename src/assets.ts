export enum Keys {
  ufo = "ufo",
  star = "star",
  alien = "alien",
  bomb = "bomb",
  explosion = "explosion",
  platform = "platform"
}

const PREFIX = "assets";
const DEFAULT_EXTENSION = "png";
const _keyToAssetPath: { [id: string]: string } = {};

function buildAssets(): void {
  for (const k in Keys) {
    const keyName = typeof k === "string" ? k : k[0];
    const extension = typeof k === "string" ? DEFAULT_EXTENSION : k[1];
    _keyToAssetPath[keyName] = `${PREFIX}/${keyName}.${extension}`;
  }
}

buildAssets();

const keyToAssetPath: { [TKey in Keys]: string } = {
  [Keys.ufo]: _keyToAssetPath.ufo,
  [Keys.star]: _keyToAssetPath.star,
  [Keys.alien]: _keyToAssetPath.alien,
  [Keys.bomb]: _keyToAssetPath.bomb,
  [Keys.explosion]: _keyToAssetPath.explosion,
  [Keys.platform]: _keyToAssetPath.platform
};

export class Assets {
  public static readonly keyToAssetPath = keyToAssetPath;
}
