import { Manager } from "./manager";
import { GameManager } from "../gameManager";
import { Types } from "../enemy/enemyTypes";
import { Enemy, DestroyedReason } from "../enemy/enemy";
import { Config } from "../../config";

/**
  Info of a type of enemy.
*/
export interface EnemyTypeInfoItem {
  /**
    Num of enemies of this type that are currently alive
  */
  numAlive: number;
  /**
    Num of enemies of this type that has been destroyed
  */
  numDestroyed: number;
  /**
    Max number of enemies of this type that can be on screen.
  */
  maxNum: number;
  /**
    The speed of the enemy increases of decreases based on speedMultiplier.
  */
  speedMultiplier: number;
  /**

  */
  speedStep: number;
  /**
    The next number of enemies needed to defeat in order to get to the "next
    tick" (on each tick, speedMultiplier increases and new enemy types are
    available to be instantiated).
  */
  nextTick: number;
  /**
    The amount nextTick increases each time.
  */
  readonly tickStep: number;
}

/**
  Manages the instantiaton of enemies.
  - The enemies are instantiated based on the enemies the player has defeated.
  - The enemies' speed increase based on the enemies defeated.
  - The max number of enemies on screen increas based on the enemies defeated.
 */
export class EnemyManager extends Manager {
  /**
    All types of enemies available to instantiate.
  */
  private static readonly ENEMY_TYPES: { [id: string]: any } = Types;
  private static readonly ENEMY_TYPES_KEYS: string[] = Object.keys(Types);
  private static readonly NUM_OF_ENEMY_TYPES: number =
    EnemyManager.ENEMY_TYPES_KEYS.length;

  private readonly initialMaxNum: number;
  private readonly speedStep: number;
  private readonly tickStep: number;
  /**
    Total number of enemies currently alive.
  */
  private globalNumAlive: number;
  /**
    Max total number of enemies that can be on screen.
  */
  private globalMaxNum: number;
  /**
    Position of the hardest enemy available on ENEMY_TYPES.
  */
  private hardestEnemyAvailablePos: number;
  /**
    Currently available enemy types (based on types of enemies unlocked and
    enemies currently on screen).
  */
  private availableTypes: string[];
  private enemyTypesInfo: { [id: string]: EnemyTypeInfoItem };

  constructor(game: GameManager, physicsSystem: number) {
    super(game, physicsSystem);

    this.initialMaxNum = Config.ENEMY.INITIAL_MAX_NUM;
    this.speedStep = Config.ENEMY.SPEED_STEP;
    this.tickStep = Config.ENEMY.TICK_STEP;
  }

  public init() {
    super.init();

    const { initialMaxNum, speedStep, tickStep } = this;

    this.enemyTypesInfo = {};
    for (const k of EnemyManager.ENEMY_TYPES_KEYS) {
      this.enemyTypesInfo[k] = {
        numAlive: 0,
        numDestroyed: 0,
        maxNum: initialMaxNum,
        speedMultiplier: 1,
        speedStep: speedStep,
        tickStep: tickStep,
        nextTick: tickStep
      };
    }
    const firstEnemyTypeKey = EnemyManager.ENEMY_TYPES_KEYS[0];
    const firstEnemyTypeInfo = this.enemyTypesInfo[firstEnemyTypeKey];
    this.globalMaxNum = firstEnemyTypeInfo.maxNum;
    this.globalNumAlive = 0;
    this.hardestEnemyAvailablePos = 0;
    this.availableTypes = [];
    this.setTypeAsAvailable(firstEnemyTypeKey);
  }

  /**
    Sets a type of enemi as available or unavailable for instantiaton.
  */
  private setTypeAsAvailable(typeName: string, available: boolean = true) {
    const pos = this.availableTypes.findIndex(t => t === typeName);
    const isInAvailableTypes = pos !== -1;

    if (isInAvailableTypes === !available) {
      if (available) {
        this.availableTypes.push(typeName);
      } else {
        // Removes the type from availableTypes
        const { availableTypes } = this;
        const lastPos = availableTypes.length - 1;
        if (availableTypes.length > 1 && availableTypes[lastPos] !== typeName) {
          let aux = availableTypes[lastPos];
          availableTypes[lastPos] = availableTypes[pos];
          availableTypes[pos] = aux;
        }
        availableTypes.pop();
      }
    }
  }

  /**
    Unlocks the next type of enemy (it's now available for instantiaton).
  */
  private unlockNextEnemyType() {
    this.hardestEnemyAvailablePos++;
    if (this.hardestEnemyAvailablePos > EnemyManager.NUM_OF_ENEMY_TYPES - 1) {
      this.hardestEnemyAvailablePos = EnemyManager.NUM_OF_ENEMY_TYPES - 1;
    } else {
      const typeName =
        EnemyManager.ENEMY_TYPES_KEYS[this.hardestEnemyAvailablePos];
      this.setTypeAsAvailable(typeName);
      const info = this.enemyTypesInfo[typeName];
      this.globalMaxNum += info.maxNum;
    }
  }

  /**
    Instantiate a random enemy based on available types.
  */
  public instantiateRandomEnemy(): Enemy {
    const random = Math.floor(
      Phaser.Math.random(0, this.availableTypes.length)
    );
    const typeName = this.availableTypes[random];
    const type = EnemyManager.ENEMY_TYPES[typeName];
    const enemy: Enemy = new type(this.game, this.physicsSystem);

    const minX = enemy.halfWidth;
    const maxX = this.game.width - enemy.halfWidth;

    const x = Math.floor(Phaser.Math.random(minX, maxX));
    const y = -Math.floor(
      Phaser.Math.random(enemy.halfHeight, 3 * enemy.height)
    );
    enemy.x = x;
    enemy.y = y;
    enemy.onDestroyed = this.onEnemyDestroyed.bind(this);

    const enemyInfo = this.enemyTypesInfo[typeName];
    enemy.setSpeedMultiplier(enemyInfo.speedMultiplier);
    enemyInfo.numAlive++;
    this.globalNumAlive++;

    if (enemyInfo.numAlive >= enemyInfo.maxNum) {
      this.setTypeAsAvailable(typeName, false);
    }

    this.group.add(enemy);

    return enemy;
  }

  public onMissileCollision(enemy: Enemy) {
    enemy.onMissileCollision();
  }

  public onShieldCollision(enemy: Enemy) {
    enemy.onShieldCollision();
    this.game.onShieldDamaged(enemy.getHarm());
  }

  public onEnemyDestroyed(enemy: Enemy, reason: DestroyedReason) {
    const typeName = enemy.constructor.name;
    const enemyInfo = this.enemyTypesInfo[typeName];
    // Don't increase difficulty unless the player was who destroyed the enemy.
    if (reason === DestroyedReason.missile) {
      enemyInfo.numDestroyed++;
    }
    enemyInfo.numAlive--;
    this.globalNumAlive--;

    // Next tick for this enemy
    if (
      reason === DestroyedReason.missile &&
      enemyInfo.numDestroyed > enemyInfo.nextTick
    ) {
      enemyInfo.maxNum++;
      enemyInfo.nextTick += enemyInfo.tickStep;
      enemyInfo.speedMultiplier += enemyInfo.speedStep;
      this.globalMaxNum++;

      // Tries to unlock a new type of enemy because the next tick has been
      // reached.
      this.unlockNextEnemyType();
    }

    // When an enemy is destroyed, set all enemy types as available.
    for (let i = 0; i <= this.hardestEnemyAvailablePos; i++) {
      this.setTypeAsAvailable(EnemyManager.ENEMY_TYPES_KEYS[i]);
    }

    // Inform the game manager that a enemy has been destroyed.
    this.game.onEnemyDestroyed(
      reason === DestroyedReason.missile ? enemy.getScore() : 0
    );
  }

  public update() {
    if (
      this.globalNumAlive < this.globalMaxNum &&
      this.availableTypes.length > 0
    ) {
      this.instantiateRandomEnemy();
    }
  }

  public onGameOver() {}

  public clean() {
    this.destroyGroupChildren();
  }
}
