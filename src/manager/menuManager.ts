import { Manager } from "./manager";
import { GameManager } from "../gameManager";
import { Config } from "../../config";

/**
  Manages the rendering and events of the game over menu.
*/
export class MenuManager extends Manager {
  constructor(game: GameManager, physicsSystem: number) {
    super(game, physicsSystem);
  }

  private render() {
    const { game } = this;
    const gameWidth = game.width;
    const gameHeight = game.height;
    const halfGameWidth = gameWidth / 2;
    const halfGameHeight = gameHeight / 2;

    this.renderWindow(halfGameWidth, halfGameHeight);
    this.renderRetryButton(halfGameWidth, halfGameHeight);
    this.renderAllTexts(halfGameWidth, halfGameHeight);
  }

  private renderWindow(windowXCenter: number, windowYCenter: number) {
    const width = windowXCenter;
    const height = windowYCenter;

    this.renderRect(
      windowXCenter,
      windowYCenter,
      width,
      height,
      Config.MENU.WINDOW.COLOR,
      Config.MENU.WINDOW.ALPHA,
      Config.MENU.WINDOW.LINE_COLOR,
      Config.MENU.WINDOW.LINE_ALPHA
    );
  }

  private renderAllTexts(windowXCenter: number, windowYCenter: number) {
    this.renderText(
      Config.MENU.GAME_OVER_TEXT.CONTENT,
      windowXCenter,
      windowYCenter - windowYCenter / 4,
      Config.MENU.GAME_OVER_TEXT.COLOR
    );
    this.renderText(
      Config.MENU.RETRY_BUTTON.TEXT.CONTENT,
      windowXCenter,
      windowYCenter + windowYCenter / 4,
      Config.MENU.RETRY_BUTTON.TEXT.COLOR
    );
  }

  private renderRetryButton(windowXCenter: number, windowYCenter: number) {
    this.renderRect(
      windowXCenter,
      windowYCenter + windowYCenter / 4,
      Config.MENU.RETRY_BUTTON.SIZE.width,
      Config.MENU.RETRY_BUTTON.SIZE.height,
      Config.MENU.RETRY_BUTTON.COLOR,
      Config.MENU.RETRY_BUTTON.ALPHA,
      Config.MENU.RETRY_BUTTON.LINE_COLOR,
      Config.MENU.RETRY_BUTTON.LINE_ALPHA,
      this.retry
    );
  }

  private renderText(content: string, x: number, y: number, color: string) {
    const text = this.game.add.text(0, 0, content);
    const textHalfWidth = text.width / 2;
    const textHalfHeight = text.height / 2;
    text.x = x - textHalfWidth;
    text.y = y - textHalfHeight;
    text.addColor(color, 0);

    this.group.add(text);
  }

  private renderRect(
    x: number,
    y: number,
    width: number,
    height: number,
    fillColor: number,
    fillAlpha: number,
    lineColor: number,
    lineAlpha: number,
    onclick: (graphics: Phaser.Graphics, color: number) => any = null
  ) {
    const graphics = this.game.add.graphics(0, 0);
    graphics.clear();
    graphics.beginFill(fillColor, fillAlpha);
    graphics.lineStyle(4, lineColor, lineAlpha);
    graphics.drawRect(x - width / 2, y - height / 2, width, height);
    graphics.endFill();

    if (onclick) {
      graphics.inputEnabled = true;
      graphics.events.onInputDown.add(onclick.bind(this));
    }

    this.group.add(graphics);
  }

  private retry() {
    this.game.restart();
  }

  public clean() {
    this.destroyGroupChildren();
  }

  public onGameOver() {
    this.render();
  }
}
