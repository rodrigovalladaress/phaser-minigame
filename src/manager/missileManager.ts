import { Manager } from "./manager";
import { GameManager } from "../gameManager";
import { Missile } from "../missile/missile";
import { Explosion } from "../missile/explosion";
import { Config } from "../../config";

/**
  Renders the missiles that the player uses to defeat enemies.
*/
export class MissileManager extends Manager {
  private readonly numOfMissiles: number;
  private readonly offset: number;
  private readonly minSpeed: number;
  private readonly maxSpeed: number;
  private readonly speedMultiplier: number;
  private readonly maxDragDistance: number;
  private readonly bounceX: number;
  private readonly bounceY: number;
  private explosionGroup: Phaser.Group;

  constructor(game: GameManager, physicsSystem: number) {
    super(game, physicsSystem);

    this.numOfMissiles = Config.MISSILE.COUNT;
    this.offset = Config.MISSILE.OFFSET;
    this.minSpeed = Config.MISSILE.MIN_SPEED;
    this.maxSpeed = Config.MISSILE.MAX_SPEED;
    this.speedMultiplier = Config.MISSILE.SPEED_MULTIPLIER;
    this.maxDragDistance = Config.MISSILE.MAX_DRAG_DISTANCE;
    this.bounceX = Config.MISSILE.BOUNCE.X;
    this.bounceY = Config.MISSILE.BOUNCE.Y;
  }

  public init() {
    super.init();

    const {
      group,
      numOfMissiles,
      game,
      physicsSystem,
      offset,
      minSpeed,
      maxSpeed,
      speedMultiplier,
      maxDragDistance,
      bounceX,
      bounceY
    } = this;
    const missiles: Missile[] = [];

    // Prevent the missiles to bounce on the top edge of the screen
    this.game.physics.arcade.checkCollision.up = false;

    for (let i = 0; i < numOfMissiles; i++) {
      const missile = new Missile(
        game,
        physicsSystem,
        minSpeed,
        maxSpeed,
        speedMultiplier,
        maxDragDistance,
        bounceX,
        bounceY
      );
      missiles.push(missile);
      group.add(missile);
    }

    group.align(
      numOfMissiles,
      1,
      game.width / numOfMissiles,
      game.height - offset,
      Phaser.BOTTOM_CENTER
    );

    for (let missile of missiles) {
      missile.saveAsInitialPosition();
    }

    this.game.physics.arcade.enable(group);

    this.explosionGroup = this.game.add.group();
  }

  public onEnemyCollision(missile: Missile) {
    const explosion = new Explosion(this.game, missile.x, missile.y);
    this.explosionGroup.add(explosion);

    missile.onEnemyCollision();
  }

  public getExplosionGroup(): Phaser.Group {
    return this.explosionGroup;
  }

  public onGameOver() {
    this.destroyGroupChildren();
  }

  public clean() {}
}
