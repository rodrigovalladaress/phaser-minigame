import { GameManager } from "./../gameManager";

/**
  Manager base class.
*/
export abstract class Manager {
  protected readonly game: GameManager;
  protected readonly physicsSystem: number;
  protected group: Phaser.Group;

  constructor(game: GameManager, physicsSystem: number) {
    this.game = game;
    this.physicsSystem = physicsSystem;
  }

  public getGroup(): Phaser.Group {
    return this.group;
  }

  public init() {
    this.clean();

    this.group = this.game.add.physicsGroup();
    this.group.enableBody = true;
  }

  public abstract onGameOver();

  protected abstract clean();

  /**
    Destroy all the sprites on this manager's group.
  */
  protected destroyGroupChildren() {
    const { group } = this;
    if (group) {
      const toDestroy: Phaser.Sprite[] = [];

      group.forEach((sprite: Phaser.Sprite) => toDestroy.push(sprite));

      while (toDestroy.length > 0) {
        const sprite = toDestroy.pop();
        sprite.destroy();
      }
    }
  }
}
