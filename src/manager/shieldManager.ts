import { Manager } from "./manager";
import { GameManager } from "../gameManager";
import { Keys } from "../assets";
import { Config } from "../../config";

/**
  Shows and updates the shield of the player. The shield fades out based on the
  damaged taken.
*/
export class ShieldManager extends Manager {
  private readonly initialLife: number;
  private readonly spriteName: string;
  private readonly porcentualDistance: number;
  private life: number;
  private shield: Phaser.Sprite;

  constructor(game: GameManager, physicsSystem: number) {
    super(game, physicsSystem);

    this.initialLife = Config.SHIELD.LIFE;
    this.spriteName = Keys.platform;
    this.porcentualDistance = Config.SHIELD.PORCENTUAL_DISTANCE;
  }

  public init() {
    super.init();

    this.shield = this.game.add.sprite(
      0,
      this.porcentualDistance * this.game.height,
      this.spriteName
    );
    this.shield.width = this.game.width;
    const { shield } = this;
    this.game.add.existing(shield);
    this.game.physics.enable(shield, this.physicsSystem);
    this.group.add(this.shield);

    this.life = this.initialLife;
  }

  private updateAlpha() {
    this.shield.alpha = this.life / this.initialLife;
  }

  public hurt(amount: number) {
    this.life = Phaser.Math.clamp(this.life - amount, 0, this.initialLife);
    this.updateAlpha();

    if (this.life === 0) {
      this.game.setGameOver();
    }
  }

  public onGameOver() {}

  public clean() {
    this.destroyGroupChildren();
  }
}
