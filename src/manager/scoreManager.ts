import { Manager } from "./manager";
import { GameManager } from "../gameManager";
import { Config } from "../../config";

/**
  Shows and updates the score of the player.
*/
export class ScoreManager extends Manager {
  private readonly textColor: string;
  private score: number;
  private text: Phaser.Text;

  constructor(game: GameManager, physicsSystem: number) {
    super(game, physicsSystem);
    this.textColor = Config.SCORE.COLOR;
  }

  public init() {
    super.init();
    this.text = this.game.add.text(0, 0, this.getScoreString());
    this.text.addColor(this.textColor, 0);
    this.group.add(this.text);

    this.score = 0;
    this.updateText();
  }

  public addScore(delta: number) {
    this.score += delta;
    this.updateText();
  }

  private updateText() {
    this.text.setText(this.getScoreString());
  }

  private getScoreString(): string {
    return `Score: ${this.score}`;
  }

  public onGameOver() {}

  public clean() {}
}
