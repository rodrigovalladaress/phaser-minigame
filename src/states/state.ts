import { GameManager } from "../gameManager";

/**
  State base class.
*/
export class State extends Phaser.State {
  protected readonly gameManager: GameManager;
  protected readonly next: string;
  protected readonly onCreated: () => any;

  constructor(gameManager: GameManager, onCreated: () => any = null) {
    super();
    this.gameManager = gameManager;
    this.onCreated = onCreated;
  }

  public create() {
    super.create(this.game);

    if (this.onCreated) {
      this.onCreated();
    }
  }
}
