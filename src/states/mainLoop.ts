import { State } from "./state";

import { GameManager } from "../gameManager";

/**
  Adds the background and send update signals to the game manager.
*/
export class MainLoop extends State {
  constructor(gameManager: GameManager, onCreated: () => any = null) {
    super(gameManager, onCreated);
  }

  public create() {
    super.create();
  }

  public update() {
    this.gameManager.checkMissilesEnemiesCollision();
    this.gameManager.updateEnemies();
    this.gameManager.checkEnemiesShieldCollision();
  }
}
