import { State } from "./state";
import { GameManager } from "../gameManager";
import { Assets } from "../assets";

/**
  Boot state. Loads assets.
*/
export class Boot extends State {
  physicsSystem: number;

  constructor(
    gameManager: GameManager,
    physicsSystem: number,
    onCreated: () => any
  ) {
    super(gameManager, onCreated);

    this.physicsSystem = physicsSystem;
  }

  public preload() {
    const { game } = this;

    console.log("Loading assets...");
    for (const k in Assets.keyToAssetPath) {
      console.log(k, " => ", Assets.keyToAssetPath[k]);
      game.load.image(k, Assets.keyToAssetPath[k]);
    }
    console.log("Assets loaded");
  }

  public create() {
    console.log("boot create");
    this.game.physics.startSystem(this.physicsSystem);
    super.create();
  }
}
