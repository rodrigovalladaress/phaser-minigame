import { Config } from "../config";

import { Manager } from "./manager/manager";
import { ScreenSizeHelper } from "./helpers/screenSizeHelper";
import { EnemyManager } from "./manager/enemyManager";
import { MissileManager } from "./manager/missileManager";
import { ScoreManager } from "./manager/scoreManager";
import { ShieldManager } from "./manager/shieldManager";
import { MenuManager } from "./manager/menuManager";

import { Boot } from "./states/boot";
import { MainLoop } from "./states/mainLoop";

enum States {
  boot = "boot",
  game = "game"
}

/**
  Manages the game states and allows other managers to interact with the game.
*/
export class GameManager extends Phaser.Game {
  public readonly physicsSystem: number;

  private enemyManager: EnemyManager;
  private missileManager: MissileManager;
  private scoreManager: ScoreManager;
  private shieldManager: ShieldManager;
  private menuManager: MenuManager;

  private managers: Manager[];

  private gameOver: boolean;

  constructor(config: Phaser.IGameConfig) {
    const screenSizeHelper = new ScreenSizeHelper(Config.SCREEN.RATIO);
    config.width = screenSizeHelper.gameRealSize.width;
    config.height = screenSizeHelper.gameRealSize.height;

    super(Object.assign(config));

    this.initStates();

    this.physicsSystem = Phaser.Physics.ARCADE;
  }

  private initStates() {
    this.state.add(
      States.boot,
      // When the boot state is created, constructManagers is called.
      new Boot(this, this.physicsSystem, () => this.constructManagers())
    );
    // When the main loop state is created, initManagers is called.
    this.state.add(States.game, new MainLoop(this, () => this.initManagers()));

    this.state.start(States.boot);
  }

  private constructManagers() {
    this.managers = [];

    this.enemyManager = new EnemyManager(this, this.physicsSystem);
    this.missileManager = new MissileManager(this, this.physicsSystem);
    this.scoreManager = new ScoreManager(this, this.physicsSystem);
    this.shieldManager = new ShieldManager(this, this.physicsSystem);
    this.menuManager = new MenuManager(this, this.physicsSystem);

    const {
      enemyManager,
      missileManager,
      scoreManager,
      shieldManager,
      menuManager
    } = this;
    this.managers = [
      enemyManager,
      missileManager,
      scoreManager,
      shieldManager,
      menuManager
    ];

    this.state.start(States.game);
  }

  private initManagers() {
    for (let manager of this.managers) {
      manager.init();
    }
  }

  public instantiateRandomEnemy() {
    this.enemyManager.instantiateRandomEnemy();
  }

  public checkMissilesEnemiesCollision() {
    const { missileManager, enemyManager, gameOver } = this;
    if (!gameOver && missileManager && enemyManager) {
      const missileGroup = missileManager.getGroup();
      const explosionGroup = missileManager.getExplosionGroup();
      const enemyGroup = enemyManager.getGroup();

      this.physics.arcade.collide(
        missileGroup,
        enemyGroup,
        (missile, enemy) => {
          missileManager.onEnemyCollision(missile);
          enemyManager.onMissileCollision(enemy);
        },
        null,
        this
      );

      this.physics.arcade.overlap(
        explosionGroup,
        enemyGroup,
        (_, enemy) => {
          // Treat the collision with an explosion as if it were with a missile.
          enemyManager.onMissileCollision(enemy);
        },
        null,
        this
      );
    }
  }

  public checkEnemiesShieldCollision() {
    const { enemyManager, shieldManager, gameOver } = this;
    if (!gameOver && enemyManager && shieldManager) {
      const enemyGroup = enemyManager.getGroup();
      const shieldGroup = shieldManager.getGroup();

      this.physics.arcade.overlap(
        shieldGroup,
        enemyGroup,
        (_, enemy) => {
          enemyManager.onShieldCollision(enemy);
        },
        null,
        this
      );
    }
  }

  public onEnemyDestroyed(score: number) {
    if (!this.gameOver) {
      this.scoreManager.addScore(score);
    }
  }

  public onShieldDamaged(amount: number) {
    if (!this.gameOver) {
      this.shieldManager.hurt(amount);
    }
  }

  public updateEnemies() {
    if (!this.gameOver) {
      this.enemyManager.update();
    }
  }

  public setGameOver() {
    this.gameOver = true;
    for (let manager of this.managers) {
      manager.onGameOver();
    }
  }

  public restart() {
    this.state.restart();
    this.gameOver = false;
  }
}
