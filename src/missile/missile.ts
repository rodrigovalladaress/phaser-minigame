import { Size } from "../interface/size";
import { Keys } from "../assets";

/**
  Missile used by the player to defeat enemies.
 */
export class Missile extends Phaser.Sprite {
  public static readonly SCALE: Size = { width: 3.5, height: 3.5 };
  public static readonly SPRITE: string = Keys.bomb;

  private readonly minSpeed: number;
  private readonly maxSpeed: number;
  private readonly speedMultiplier: number;
  /**
    Distance that the missile can be dragged before it is automatically
    launched.
  */
  private readonly maxDragDistance: number;

  private initialPosition: Phaser.Point;

  private dragStartPosition: Phaser.Point;

  constructor(
    game: Phaser.Game,
    physicsSystem: number,
    minSpeed: number,
    maxSpeed: number,
    speedMultiplier: number,
    maxDragDistance: number,
    bounceX: number,
    bounceY: number
  ) {
    super(game, 0, 0, Missile.SPRITE);

    this.minSpeed = minSpeed;
    this.maxSpeed = maxSpeed;
    this.speedMultiplier = speedMultiplier;
    this.maxDragDistance = maxDragDistance;

    this.scale.setTo(Missile.SCALE.width, Missile.SCALE.height);

    this.initPhysics(physicsSystem, bounceX, bounceY);
    this.initEvents();
    this.resetMissile();
  }

  private initPhysics(physicsSystem: number, bounceX: number, bounceY: number) {
    this.game.physics.enable(this, physicsSystem);
    this.game.add.existing(this);
    this.body.collideWorldBounds = true;
    this.body.bounce.setTo(bounceX, bounceY);

    this.checkWorldBounds = true;
  }

  private initEvents() {
    this.events.onDragStart.add(this.onDragStart.bind(this));
    this.events.onDragUpdate.add(this.onDragUpdate.bind(this));
    this.events.onDragStop.add(this.onDragStop.bind(this));

    this.events.onOutOfBounds.add(this.resetMissile.bind(this));
  }

  /**
    Saves the position as initial in order to use it when the missile is
    reseted.
  */
  public saveAsInitialPosition() {
    this.initialPosition = new Phaser.Point(this.x, this.y);
  }

  /**
    Moves the missile to the initial position and enables drag.
  */
  private resetMissile() {
    const { initialPosition } = this;

    this.inputEnabled = true;
    this.input.enableDrag();

    if (initialPosition) {
      this.x = initialPosition.x;
      this.y = initialPosition.y;
    }
    this.body.velocity.x = 0;
    this.body.velocity.y = 0;
  }

  private onDragStart(_, { position }) {
    this.dragStartPosition = new Phaser.Point(position.x, position.y);
  }

  private onDragUpdate(_, pointer) {
    const { dragStartPosition, maxDragDistance } = this;

    if (dragStartPosition) {
      const { position, timeDown } = pointer;

      const distance = Phaser.Math.distance(
        position.x,
        position.y,
        dragStartPosition.x,
        dragStartPosition.y
      );

      if (distance >= maxDragDistance) {
        this.onDragStop(_, {
          position,
          timeDown,
          timeUp: new Date().getTime(),
          distance
        });
      }
    }
  }

  private onDragStop(_, { position, timeDown, timeUp, distance = null }) {
    const { dragStartPosition, minSpeed, maxSpeed, speedMultiplier } = this;
    if (!distance) {
      distance = Phaser.Math.distance(
        position.x,
        position.y,
        dragStartPosition.x,
        dragStartPosition.y
      );
    }

    const dragStopPosition = new Phaser.Point(position.x, position.y);
    const xDir = dragStopPosition.x - dragStartPosition.x;
    const yDir = dragStopPosition.y - dragStartPosition.y;
    let dir: Phaser.Point = new Phaser.Point(xDir, yDir).normalize();
    const time = timeUp - timeDown;
    const speed = (distance / time) * speedMultiplier;
    const clampedSpeed = Phaser.Math.clamp(speed, minSpeed, maxSpeed);

    dir = dir.expand(clampedSpeed);

    this.throwMissile(dir.x, dir.y);
  }

  public onEnemyCollision() {
    this.resetMissile();
  }

  /**
    Launches the missile. While it's in the "launch state", the player can't
    interact with it.
  */
  private throwMissile(speedX, speedY) {
    this.inputEnabled = false;
    this.input.disableDrag();

    this.body.velocity.x = speedX;
    this.body.velocity.y = speedY;
  }
}
