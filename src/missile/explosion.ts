import { Size } from "../interface/size";
import { Keys } from "../assets";

/**
  Explosion sprite instantiated when the missile explodes.
*/
export class Explosion extends Phaser.Sprite {
  public static readonly SCALE: Size = { width: 0.8, height: 0.8 };
  public static readonly SPRITE: string = Keys.explosion;
  public static readonly EXPLOSION_DURATION: number = 2;

  private readonly tween: Phaser.Tween;

  constructor(game: Phaser.Game, x: number, y: number) {
    super(game, x, y, Explosion.SPRITE);

    this.scale.setTo(Explosion.SCALE.width, Explosion.SCALE.height);
    this.anchor.set(0.5, 0.5);

    this.game.add.existing(this);
    this.game.physics.enable(this, Phaser.Physics.ARCADE);

    this.tween = game.add.tween(this);

    this.tween.to({ alpha: 0 }, 500, Phaser.Easing.Linear.None, true);
    this.tween.onComplete.add(this.onExplosionEnded.bind(this));
  }

  onExplosionEnded() {
    this.destroy();
    this.tween.onComplete.remove(this.onExplosionEnded);
  }
}
