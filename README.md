# phaser-minigame

Enemies appear at the top of the screen. They move towards to the planet's shield, that the player needs to protect. To do that, there are 3 missiles available at the bottom of the screen, that the player can drag into the enemies in order to defeat them. The shield fades out when it gets damage, and the game ends when the shield is destroyed.

![picture](sample.PNG)

  * Technologies:
    * [Phaser.io](https://phaser.io/)
    * [TypeScript](https://www.typescriptlang.org/)
    * [Webpack](https://webpack.js.org/)
  * Webpack and TypeScript configuration is based on [Phaser NPM Webpack TypeScript Starter Project](https://github.com/rroylance/phaser-npm-webpack-typescript-starter-project).
  * Image assets are from [Phaser examples](https://github.com/photonstorm/phaser-examples)